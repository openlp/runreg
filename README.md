# runreg

[![PyPI - Version](https://img.shields.io/pypi/v/runreg.svg)](https://pypi.org/project/runreg)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/runreg.svg)](https://pypi.org/project/runreg)

-----

**Table of Contents**

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install runreg
```

## License

`runreg` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
