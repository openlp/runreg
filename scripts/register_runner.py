#!/usr/bin/env python3
from argparse import ArgumentParser
from subprocess import run

import requests


def parse_args():
    parser = ArgumentParser()
    parser.add_argument('-u', '--url', metavar='URL', help='The base URL of the runreg server', required=True)
    parser.add_argument('-r', '--runner-type-id', metavar='RUNNER_TYPE_ID', help='The ID of the type of this runner',
                        required=True)
    parser.add_argument('-t', '--token', metavar='TOKEN', help='The authentication token', required=True)
    return parser.parse_args()


def get_runners(url: str, runner_type_id: str, token: str) -> list[dict]:
    response = requests.get(url + '/runners/unregistered/' + runner_type_id,
                            headers={'Authorization': f'Bearer {token}'})
    if response.status_code == 200:
        return response.json()['runners']
    else:
        print(response)
    return []


def build_args(runner):
    """Build the registration command line arguments"""
    args = ['register', '--url', 'https://gitlab.com', '--executor', 'shell', '--non-interactive']
    if runner.get('name'):
        args.extend(['--name', runner['name']])
    if runner.get('token'):
        args.extend(['--token', runner['token']])
    return args


def register_runner(runner: dict, url: str, token: str):
    print(f'Registering runner {runner["id"]} - {runner["runner_type"]["name"]}...')
    try:
        result = run(['gitlab-runner'] + build_args(runner), capture_output=True)
        has_run = True
    except OSError:
        result = None
        has_run = False
    if not has_run or (result and result.returncode != 0):
        requests.post(f'{url}/runners/{runner["id"]}/set-unregistered',
                      headers={'Authorization': f'Bearer {token}'})
        if result:
            print(result.stderr)
        else:
            print('Unable to run registration command')
    else:
        requests.post(url + f'/runners/{runner["id"]}/set-active',
                      headers={'Authorization': f'Bearer {token}'})
        print('Registration successful')


def main():
    args = parse_args()
    runners = get_runners(args.url, args.runner_type_id, args.token)
    for runner in runners:
        register_runner(runner, args.url, args.token)


if __name__ == '__main__':
    main()
