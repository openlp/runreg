from flask_login import LoginManager
from sqlalchemy.sql.expression import select

from runreg.database import session
from runreg.models import User


login_manager = LoginManager()
login_manager.login_view = 'auth.login'


@login_manager.user_loader
def user_loader(email):
    with session() as s:
        user = s.scalars(select(User).where(User.email == email, User.is_active == True)).first()  # noqa: E712
    return user


@login_manager.request_loader
def request_loader(request):
    if request.authorization:
        with session() as s:
            user = s.scalars(select(User).where(User.access_token == request.authorization.token,
                                                User.is_active == True)).first()  # noqa: E712
        return user
    return None
