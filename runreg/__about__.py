# SPDX-FileCopyrightText: 2023-present Raoul Snyman <raoul@snyman.info>
#
# SPDX-License-Identifier: MIT
__version__ = '0.0.1'
