from flask_login import login_user, logout_user
from quart import Blueprint, request, redirect
from quart.templating import render_template as render
from sqlalchemy.sql.expression import select

from runreg.bcrypt import bcrypt
from runreg.database import session
from runreg.models import User

auth = Blueprint('auth', __name__)


@auth.route('/login', methods=['GET', 'POST'])
async def login():
    if request.method == 'POST':
        form = await request.form
        if form.get('email') and form.get('password'):
            with session() as s:
                user = s.scalars(select(User).where(User.email == form.get('email'))).first()
            if user and bcrypt.check_password_hash(user.password, form.get('password')):
                login_user(user)
                return redirect('/runners')
            else:
                print(user)
    return await render('login.html')


@auth.route('/logout', methods=['GET'])
async def logout():
    logout_user()
    return redirect('/')
