from quart import Blueprint, request, redirect
from quart.templating import render_template as render
from flask_login import login_required, current_user
from sqlalchemy.sql.expression import select

from runreg.database import session
from runreg.models import Runner, RunnerType, Status


runners = Blueprint('runners', __name__, url_prefix='/runners')


@runners.route('', methods=['GET'])
@login_required
async def all():
    """Return all the runners"""
    with session() as s:
        rs = s.scalars(select(Runner).where(Runner.user == current_user)).all()
        return await render('runners.html', runners=rs)


@runners.route('/register', methods=['GET', 'POST'])
@login_required
async def register():
    """Register a new runner"""
    if request.method == 'POST':
        form = await request.form
        if form.get('runner-type') and form.get('token'):
            with session() as s:
                runner_type = s.get(RunnerType, form.get('runner-type'))
                runner = Runner(
                    name=form.get('name'),
                    description=form.get('description'),
                    token=form.get('token'),
                    runner_type=runner_type,
                    user=current_user
                )
                s.add(runner)
                s.commit()
            return redirect('/runners')
    with session() as s:
        runner_types = s.scalars(select(RunnerType)).all()
        return await render('register.html', runner_types=runner_types)


@runners.route('/unregistered/<int:runner_type_id>', methods=['GET'])
@login_required
async def unregistered(runner_type_id):
    """Return all the runners"""
    runners_list = []
    with session() as s:
        runner_type = s.get(RunnerType, runner_type_id)
        if not runner_type:
            return {'runners': []}
        rs = s.scalars(select(Runner).where(Runner.user == current_user, Runner.runner_type == runner_type,
                                            Runner.status == Status.Unregistered)).all()
        for runner in rs:
            d = runner.to_dict()
            d['runner_type'] = runner.runner_type.to_dict()
            d['status'] = d['status'].name
            d['runner_type']['tags'] = [tag.to_dict() for tag in runner.runner_type.tags]
            runners_list.append(d)
            # Update status to pending
            runner.status = Status.Pending
        s.add_all(rs)
        s.commit()
    return {'runners': runners_list}


@runners.route('/<int:runner_id>/set-active', methods=['POST'])
@login_required
async def set_active(runner_id):
    with session() as s:
        runner = s.get(Runner, runner_id)
        if runner:
            runner.status = Status.Active
            s.add(runner)
            s.commit()
            pass
        else:
            return 'Not found', 404
    return ''


@runners.route('/<int:runner_id>/set-unregistered', methods=['POST'])
@login_required
async def set_unregistered(runner_id):
    with session() as s:
        runner = s.get(Runner, runner_id)
        if runner:
            runner.status = Status.Unregistered
            s.add(runner)
            s.commit()
            pass
        else:
            return 'Not found', 404
    return ''
