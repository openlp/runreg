from enum import Enum
from typing import Optional

from flask_login import UserMixin
from quart_sqlalchemy.model.mixins import SimpleDictMixin
from sqlalchemy.orm import Mapped, mapped_column, relationship            # type: ignore[attr-defined]
from sqlalchemy.schema import Column, ForeignKey, Table
from sqlalchemy.types import Integer, String, Enum as SQLEnum

from runreg.database import Model


runner_types_tags_table = Table(
    'runner_types_tags',
    Model.metadata,
    Column('runner_type_id', ForeignKey('runner_types.id')),
    Column('tag_id', ForeignKey('tags.id'))
)


class Status(Enum):
    Unregistered = 'unregistered'
    Pending = 'pending'
    Active = 'active'
    Deleted = 'deleted'


class User(Model, UserMixin):
    __tablename__ = 'users'

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    email: Mapped[str] = mapped_column(String(255))
    password: Mapped[Optional[str]] = mapped_column(String(255))
    access_token: Mapped[Optional[str]] = mapped_column(String(255))
    is_active: Mapped[bool] = mapped_column(default=True)

    runners: Mapped[list['Runner']] = relationship('Runner', back_populates='user')

    def get_id(self):
        return self.email


class Tag(Model, SimpleDictMixin):
    __tablename__ = 'tags'

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    name: Mapped[str] = mapped_column(String(255))


class RunnerType(Model, SimpleDictMixin):
    __tablename__ = 'runner_types'

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    name: Mapped[str] = mapped_column(String(255))

    tags: Mapped[list[Tag]] = relationship(Tag, secondary=runner_types_tags_table)


class Runner(Model, SimpleDictMixin):
    __tablename__ = 'runners'

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    runner_type_id: Mapped[int] = mapped_column(Integer, ForeignKey('runner_types.id'))
    user_id: Mapped[int] = mapped_column(Integer, ForeignKey('users.id'))
    token: Mapped[str] = mapped_column(String(255))
    name: Mapped[str] = mapped_column(String(255))
    description: Mapped[Optional[str]] = mapped_column(String(255))
    status: Mapped[str] = mapped_column(SQLEnum(Status), default=Status.Unregistered)

    runner_type: Mapped[RunnerType] = relationship(RunnerType)
    user: Mapped[User] = relationship(User, back_populates='runners')
