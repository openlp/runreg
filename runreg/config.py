import os
from typing import Any

from quart.config import Config as QuartConfig
from quart_sqlalchemy import SQLAlchemyConfig

DEFAULT_CONFIG: dict[str, Any] = {
    'SECRET_KEY': 'changeme',
    'SQLALCHEMY_DATABASE_URI': 'sqlite://',
    'SQLALCHEMY_ECHO': False,
    'SQLALCHEMY_CHECK_SAME_THREAD': False,
    'SQLALCHEMY_EXPIRE_ON_COMMIT': False
}


def get_quart_config(config: QuartConfig):
    """Build a Quart config object and return it"""
    config.from_mapping(DEFAULT_CONFIG)
    config.from_prefixed_env('RUNREG')
    env_vars: dict[str, str] = {}
    for key, value in os.environ.items():
        if key.startswith('SQLALCHEMY_'):
            env_vars[key] = value
    config.from_mapping(env_vars)


def get_sqla_config() -> SQLAlchemyConfig:
    """Build an SQLAlchemyConfig object and return it"""
    url = os.getenv('SQLALCHEMY_DATABASE_URI') or DEFAULT_CONFIG['SQLALCHEMY_DATABASE_URI']
    echo = os.getenv('SQLALCHEMY_ECHO') or DEFAULT_CONFIG['SQLALCHEMY_ECHO']
    connect_args: dict[str, Any] = {}
    if url.startswith('sqlite'):
        connect_args['check_same_thread'] = (
            os.getenv('SQLALCHEMY_CHECK_SAME_THREAD') or DEFAULT_CONFIG['SQLALCHEMY_CHECK_SAME_THREAD']
        )
    expire_on_commit = (
        os.getenv('SQLALCHEMY_EXPIRE_ON_COMMIT') or DEFAULT_CONFIG['SQLALCHEMY_EXPIRE_ON_COMMIT']
    )
    config = SQLAlchemyConfig(
        binds={
            'default': {
                'engine': {'url': url, 'echo': echo, 'connect_args': connect_args},
                'session': {'expire_on_commit': expire_on_commit},
            }
        }
    )
    return config
