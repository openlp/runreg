from contextlib import contextmanager

from quart_sqlalchemy.framework import QuartSQLAlchemy

from runreg.config import get_sqla_config


db = QuartSQLAlchemy(config=get_sqla_config())
Model = db.Model
Session = db.bind.Session


@contextmanager
def session():
    with Session() as sess:
        with sess.begin():
            yield sess
