from runreg.app import make_app


app = make_app()
app.run(debug=True)
