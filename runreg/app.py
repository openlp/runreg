import quart_flask_patch                    # noqa: F401
from quart import Quart

from runreg.auth import login_manager
from runreg.database import db
from runreg.config import get_quart_config
from runreg.views.auth import auth
from runreg.views.runners import runners


def make_app():
    """A function to create the application object"""
    app = Quart('runreg')
    get_quart_config(app.config)
    db.init_app(app)
    db.create_all()
    login_manager.init_app(app)

    app.register_blueprint(auth)
    app.register_blueprint(runners)
    return app
