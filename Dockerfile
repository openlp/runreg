FROM python:3.11-alpine

WORKDIR /app
RUN apk add git
RUN python -m venv /app/venv
RUN /app/venv/bin/pip install -U pip wheel psycopg2-binary
COPY . /app
RUN /app/venv/bin/pip install .

CMD ["/app/venv/bin/hypercorn", "--bind", "0.0.0.0:8000", "runreg.run:app"]
